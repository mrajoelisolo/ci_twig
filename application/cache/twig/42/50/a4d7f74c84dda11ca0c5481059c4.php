<?php

/* index.twig */
class __TwigTemplate_4250a4d7f74c84dda11ca0c5481059c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>
<head>
\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">    
\t<title></title>
</head>

<body>
    <h1>Twig test</h1>
\t<p>Mon nom est ";
        // line 10
        if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
        echo twig_escape_filter($this->env, $_name_, "html", null, true);
        echo "</p>
    <strong>Persons list :</strong>
    <ul>
    ";
        // line 13
        if (isset($context["persons"])) { $_persons_ = $context["persons"]; } else { $_persons_ = null; }
        if (twig_test_iterable($_persons_)) {
            // line 14
            echo "        ";
            if (isset($context["persons"])) { $_persons_ = $context["persons"]; } else { $_persons_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_persons_);
            foreach ($context['_seq'] as $context["_key"] => $context["person"]) {
                // line 15
                echo "            <li>";
                if (isset($context["person"])) { $_person_ = $context["person"]; } else { $_person_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_person_, "getFirstName", array(), "method"), "html", null, true);
                echo " ";
                if (isset($context["person"])) { $_person_ = $context["person"]; } else { $_person_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_person_, "getLastName", array(), "method"), "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['person'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "    ";
        }
        // line 18
        echo "    </ul>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 18,  59 => 17,  46 => 15,  40 => 14,  37 => 13,  30 => 10,  19 => 1,);
    }
}
