<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {
	public function index()
	{
        $persons = array();
       
        $person = new Person();
        $person->setFirstName('Jane');
        $person->setLastName('Doe');
        $persons[] = $person;
        
        $person = new Person();
        $person->setFirstName('Jack');
        $person->setLastName('Stone');
        $persons[] = $person;
       
        $data = array(
            'name' => 'John Mad Doe',
            'persons' => $persons,
        );
        $this->twig_spark->parse('index', $data, TRUE);
	}
}

class Person
{
    private $firstName;
    private $lastName;

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($value)
    {
        $this->firstName = $value;
    }
    
    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($value)
    {
        $this->lastName = $value;
    }
}

/* End of file homepage.php */
/* Location: ./application/controllers/homepage.php */